<?php

ini_set('display_errors',1);
ini_set('display_startup_erros',1);

require 'config/config.php';
require 'app/core/Core.php';
require 'vendor/autoload.php';

$core = new Core;
$core->run();