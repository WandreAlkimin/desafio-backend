<main class="">
    <div class="header-list-page">
        <h1 class="title">Categories</h1>
        <a href="/category/add" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
        <tr class="data-row">
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Name</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Code</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Actions</span>
            </th>
        </tr>

        <?php foreach($categories as $key => $category) { ?>

        <tr class="data-row">
            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?php  echo $category['name'] ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?php  echo $category['code'] ?></span>
            </td>

            <td class="data-grid-td">
                <div class="actions">
                    <div class="action edit">
                        <a href="/category/edit/<?php  echo $category['id'] ?>" class="action back">Edit</a>
                    </div><br>
                    <div class="action delete">
                        <a href="/category/delete/<?php  echo $category['id'] ?>" class="action back">Delete</a>
                    </div>
                </div>
            </td>
        </tr>
        <?php } ?>
    </table>
</main>