<main class="content">
    <h1 class="title new-item">New Product</h1>

    <form action="/category/save" method="post">

        <div class="input-field">
            <label for="sku" class="label">Product Code</label>
            <input type="text" id="sku" class="input-text" name="code" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" class="input-text" name="name" />
        </div>

        <div class="actions-form">
            <a href="/category/show" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save Category" />
        </div>

    </form>
</main>