<main class="content">
    <h1 class="title new-item">Editando Categoria</h1>

    <form action="/category/save" method="post">

        <input type="hidden"  name="idCategory" value="<?php echo isset($category[0]['id'])? $category[0]['id']:''  ?>" />

        <div class="input-field">
            <label for="code" class="label">Product Code</label>
            <input type="text" id="code" class="input-text" name="code" value="<?php echo isset($category[0]['code'])? $category[0]['code']:''  ?>" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" class="input-text" name="name"  value="<?php echo isset($category[0]['name'])? $category[0]['name']:''  ?>" />
        </div>

        <div class="actions-form">
            <a href="/category/show" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save" />
        </div>

    </form>
</main>