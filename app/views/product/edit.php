<main class="content">
    <h1 class="title new-item">Edit Product</h1>

    <form action="/product/save" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo isset($product[0]['id'])? $product[0]['id']:''  ?>" />

        <div class="input-field">
            <input type="file" name="img" >
        </div>

        <div class="input-field label">
            <img src="<?php  echo "/assets/images/product/".$product[0]['img'] ?>" style="width: 500px" >
        </div>

        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="sku" class="input-text" name="sku" value="<?php echo isset($product[0]['sku'])? $product[0]['sku']:''  ?>" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" class="input-text" name="name" value="<?php echo isset($product[0]['name'])? $product[0]['name']:''  ?>" />
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="price" class="input-text" name="price" value="<?php echo isset($product[0]['price'])? $product[0]['price']:''  ?>" />
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="quantity" class="input-text" name="quantity" value="<?php echo isset($product[0]['quantity'])? $product[0]['quantity']:''  ?>" />
        </div>
        <div class="input-field">
            <label for="category" class="label">Categories</label>
            <select multiple id="category" class="input-text" name="category[]">
                <?php
                    foreach($categories as $category){

                        if($category['select']) {
                            echo "<option selected value=" . $category['id'] . " > ". $category['name'] ."</option>";
                        }else{
                            echo "<option value=" . $category['id'] . " > ". $category['name'] ."</option>";
                        }

                    }
                ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="description" class="input-text" name="description" ><?php echo isset($product[0]['description'])? $product[0]['description']:''  ?></textarea>
        </div>


        <div class="actions-form">
            <a href="/product/show" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save Product" />
        </div>

    </form>
</main>