<!-- Main Content -->
<main class="">
    <div class="header-list-page">
        <h1 class="title">Products</h1>
        <a href="/product/add" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
        <tr class="data-row">
            <th class="data-grid-th">

            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Name</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">SKU</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Price</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Quantity</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Categories</span>
            </th>

            <th class="data-grid-th">
                <span class="data-grid-cell-content">Actions</span>
            </th>
        </tr>
        <?php foreach($products as $key => $product) { ?>
        <tr class="data-row">

            <td class="data-grid-td">
                <img src="<?php  echo "/assets/images/product/".$product['img'] ?>" style="width: 150px" >
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?php  echo $product['name'] ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?php  echo $product['sku'] ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content">R$ <?php  echo $product['price'] ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?php  echo $product['quantity'] ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content">
                    <?php
                        foreach($product['categories'] as $category){
                            echo $category['name']."<Br />";
                        }
                    ?>

                </span>
            </td>

            <td class="data-grid-td">
                <div class="actions">
                    <div class="action edit">
                        <a href="/product/edit/<?php  echo $product['id'] ?>" class="action back">Edit</a>
                    </div><br>
                    <div class="action delete">
                        <a href="/product/delete/<?php  echo $product['id'] ?>" class="action back">Delete</a>
                    </div>
                </div>
            </td>
        </tr>
        <?php } ?>

    </table>
</main>