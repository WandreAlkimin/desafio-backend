<?php

namespace app\models;
use app\core\Model;

class Product extends Model {

    public function __construct() {
        // Chama classe mãe
        parent::__construct();
    }

    public function all(){

        $sql    = "
SELECT product.id, product.name,product.sku,product.price, product.description, product.quantity,product.img, category.id AS idCategory, category.name AS nameCategory
FROM product
LEFT JOIN category_has_product ON (product.id = category_has_product.product_id)
LEFT JOIN category ON (category_has_product.category_id = category.id)
                  ";

        $result = $this->db->query($sql);

        return $result->fetchAll();
    }


    public function store($dados){

        $sql = "insert into product SET sku = :sku, name = :name, price = :price, quantity = :quantity, description = :description, img = :img";

        $query = $this->db->prepare($sql);
        $query->bindValue(":sku",$dados['sku']);
        $query->bindValue(":name",$dados['name']);
        $query->bindValue(":price",$dados['price']);
        $query->bindValue(":quantity",$dados['quantity']);
        $query->bindValue(":description",$dados['description']);
        $query->bindValue(":img",$dados['img']);

        $query->execute();
        $productId = $this->db->lastInsertId();


        // Monta sql para inserção
        $sql = "INSERT INTO category_has_product (product_id,category_id) VALUES";

        foreach ($dados['category'] as $categoryId) {
            $sql .= " ('{$productId}', '{$categoryId}'),";
        }

        // Tira a vírgula extra
        $sql = substr($sql, 0, -1);

        $this->db->query($sql);


        return ;
    }

    public function find($id){

        $result = [];

        $sql = "
        SELECT product.id, product.name,product.sku,product.price, product.description, product.quantity,product.img, category.id AS idCategory, category.name AS nameCategory
FROM product
LEFT JOIN category_has_product ON (product.id = category_has_product.product_id)
LEFT JOIN category ON (category_has_product.category_id = category.id) where product.id = :id
        ";
        $query = $this->db->prepare($sql);
        $query->bindValue(":id",$id);
        $query->execute();

        if($query->rowCount() > 0){
            $result = $query->fetchAll();
        }

        return $result;
    }


    public function update($dados){

        $sql = "update product SET sku = :sku, name = :name, price = :price, quantity = :quantity, description = :description, img = :img where id = :id";

        $query = $this->db->prepare($sql);
        $query->bindValue(":id",$dados['id']);
        $query->bindValue(":sku",$dados['sku']);
        $query->bindValue(":name",$dados['name']);
        $query->bindValue(":price",$dados['price']);
        $query->bindValue(":quantity",$dados['quantity']);
        $query->bindValue(":description",$dados['description']);
        $query->bindValue(":img",$dados['img']);

        $query->execute();
        $productId = $dados['id'];

        // Deleta os que já existem para depois inserir, dessa forma não terá dados repetidos
        $sqlDelete = "DELETE FROM category_has_product WHERE product_id = $productId";
        $this->db->query($sqlDelete);

        // Monta sql para inserção
        $sql = "INSERT INTO category_has_product (product_id,category_id) VALUES";

        foreach ($dados['category'] as $categoryId) {
            $sql .= " ('{$productId}', '{$categoryId}'),";
        }

        // Tira a vírgula extra
        $sql = substr($sql, 0, -1);
        $this->db->query($sql);


        return ;
    }

    public function delete($id){

        //Deleta o relacionamento do produto e categoria
        $sqlDelete = "DELETE FROM category_has_product WHERE product_id = :id";
        $query = $this->db->prepare($sqlDelete);
        $query->bindValue(":id",$id);
        $query->execute();

        //Deleta o produto
        $sql = "DELETE FROM product WHERE id = :id";
        $query = $this->db->prepare($sql);
        $query->bindValue(":id",$id);
        $query->execute();


        return;
    }
}
