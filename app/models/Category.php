<?php

namespace app\models;
use app\core\Model;

class Category extends Model {

    public function __construct() {
        // Chama classe mãe
        parent::__construct();
    }

    public function all(){
        $sql    = "SELECT * FROM category";
        $result = $this->db->query($sql);

        return $result->fetchAll();
    }

    public function find($id){

        $result = [];

        $sql = "select * from category where id = :id";
        $query = $this->db->prepare($sql);
        $query->bindValue(":id",$id);
        $query->execute();

        if($query->rowCount() > 0){
            $result = $query->fetchAll();
        }

        return $result;
    }

    public function store($dados){

        $sql = "insert into category SET name = :name, code = :code";

        $query = $this->db->prepare($sql);
        $query->bindValue(":name",$dados['name']);
        $query->bindValue(":code",$dados['code']);

        $query->execute();


        return $this->db->lastInsertId();
    }

    public function update($dados){

        $sql = "update category SET name = :name, code = :code where id = :id";

        $query = $this->db->prepare($sql);
        $query->bindValue(":id"  ,$dados['id']);
        $query->bindValue(":name",$dados['name']);
        $query->bindValue(":code",$dados['code']);

        $query->execute();

        return $this->db->lastInsertId();
    }

    public function delete($id){

        $result = false;

        $sql = "select * from category_has_product where category_id = :id";
        $query = $this->db->prepare($sql);
        $query->bindValue(":id",$id);
        $query->execute();

        if($query->rowCount() == 0){

            //Deleta a categoria
            $sql = "DELETE FROM category WHERE id = :id";
            $query = $this->db->prepare($sql);
            $query->bindValue(":id",$id);
            $query->execute();
            $result = true;
        }

        return $result;
    }


}
