<?php
namespace app\core;

class Controller {

    // Chama a view e faz o include dela
    public function load($viewName, $viewDados = array() ){
        extract($viewDados);
        include "app/views/" . $viewName .".php";
    }

}