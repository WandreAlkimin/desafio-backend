<?php
namespace app\controllers;

use app\core\Controller;
use app\models\Category;

class CategoryController extends Controller{

    public function show(){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'category/index';

        $data = new Category();
        $dados["categories"] = $data->all();

        $this->load("layout/app",$dados);
    }

    public function add(){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'category/add';

        $this->load("layout/app",$dados);

    }

    public function save(){

        $data = new Category();

        $date['id']   = isset( $_POST['idCategory']) ? $_POST['idCategory']: null;
        $date['code'] = isset( $_POST['code']) ? $_POST['code']: null;
        $date['name'] = isset( $_POST['name']) ? $_POST['name']: null;

        if($date['id']){
            $data->update($date);
        }else{
            $data->store($date);
        }


        header("location:/category/show");
    }

    public function edit($id){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'category/edit';

        $data = new Category();

        $dados['category'] = $data->find($id);

        $this->load("layout/app",$dados);

    }

    public function delete($id){

        $data = new Category();
        $data->delete($id);

        header("location:/category/show");
    }

}