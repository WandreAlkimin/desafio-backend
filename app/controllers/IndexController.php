<?php
namespace app\controllers;

use app\core\Controller;

class IndexController extends Controller{

    public function index(){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'index';

        $this->load("layout/app",$dados);


    }
}