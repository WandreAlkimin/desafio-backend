<?php
namespace app\controllers;

use app\core\Controller;
use app\models\Category;
use app\models\Product;

class ProductController extends Controller{

    public function show(){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'product/index';

        $product = new Product;
        $products = $product->all();

//        echo "<pre>";
//        var_dump($products);

        // Refaz o array dos produtos colocando as categorias em somente um produto
        $pro = $this->replaceArray($products);

        $dados["products"] = $pro;

        $this->load("layout/app",$dados);
    }

    public function edit($id){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'product/edit';

        $category = new Category();
        $dados["categories"] = $category->all();

        $product = new Product();
        $dados['product'] = $product->find($id);

        $dados['product'] = $this->replaceArray($dados['product']);

        $dados["categories"] = $this->verificaSelected($dados["categories"],$dados['product'][0]['categories']);


        $this->load("layout/app",$dados);

    }

    // Mostra view para adicionar um novo produto
    public function add(){

        // Passa para o app.php o nome do arquivo para mostrar
        $dados['view'] = 'product/add';

        $data = new Category();
        $dados["categories"] = $data->all();

        $this->load("layout/app",$dados);

    }

    public function save(){
        echo '<pre>';
        $date['id']          = isset( $_POST['id'])          ? $_POST['id']: null;
        $date['sku']         = isset( $_POST['sku'])         ? $_POST['sku']: null;
        $date['name']        = isset( $_POST['name'])        ? $_POST['name']: null;
        $date['price']       = isset( $_POST['price'])       ? $_POST['price']: null;
        $date['quantity']    = isset( $_POST['quantity'])    ? $_POST['quantity']: null;
        $date['category']    = isset( $_POST['category'])    ? $_POST['category']: null;
        $date['description'] = isset( $_POST['description']) ? $_POST['description']: null;

        $nameImg = $this->uploadImg();

        $date['img'] = isset( $nameImg) ? $nameImg: null;

        $data = new Product();

        if($date['id']){
            $data->update($date);
        }else{
            $data->store($date);
        }

        header("location:/product/show");
    }

    public function delete($id){

        $data = new Product();
        $data->delete($id);

        header("location:/product/show");
    }



    public function arrayKeyExist($product,$pro){
        $keyPro = null;
        foreach($pro as $keyP => $p){

            if(isset($p['name']) && $p['name'] == $product['name']){
                $keyPro = $keyP;
            }
        }

        return $keyPro;
    }

    public function replaceArray($products){

        $pro = [];

        // Refaz o array dos produtos colocando as categorias em somente um produto
        foreach ($products as $key => $product){

            $keyPro = $this->arrayKeyExist($product,$pro);

            if(isset($keyPro)){

                $pro[$keyPro]['categories'][$key]['id']   = $product['idCategory'];
                $pro[$keyPro]['categories'][$key]['name'] = $product['nameCategory'];

            }else {

                $pro[$key] = $product;

                unset($pro[$key]['idCategory']);
                unset($pro[$key]['nameCategory']);

                $pro[$key]['categories'][$key]['id']   = $product['idCategory'];
                $pro[$key]['categories'][$key]['name'] = $product['nameCategory'];

            }
        }

        return $pro;
    }

    // Marca a lista de categorias com um selected para facilitar o front
    public function verificaSelected($categories, $categoriasSelecionadas){

        foreach($categories as $key => $category){

            $categories[$key]['select'] = false;

            foreach($categoriasSelecionadas as $selecionada){

                if($category['id'] == $selecionada['id']){
                    $categories[$key]['select'] = true;
                }
            }

        }

        return $categories;
    }

    public function uploadImg(){

        $diretorio = '/var/www/desafio-backend/assets/images/product/';
        $imageFileType = strtolower(pathinfo($_FILES['img']['name'],PATHINFO_EXTENSION));
        $name = md5($_FILES['img']['name']);

        $file = $diretorio . $name.".".$imageFileType;

        if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ) {

            if (move_uploaded_file($_FILES['img']['tmp_name'], $file)) {
                return $name.".".$imageFileType;
            }
        }

        return null;
    }
}